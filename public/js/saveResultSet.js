function saveResultSet(logApiId, url, csrf) {
  $.ajax({
    url:		url,
    type:		"POST",
    headers: {
      'X-CSRF-Token': csrf,
    },
    data: {logApiId : logApiId},
    success:	function(data, textStatus, jqXHR) {
      alert('Everything has been saved')
    },
    error:		function ($xhr,textStatus,jqXHR) {
      alert('Something went wrong')
    }
  });
}

'use strict';

let Datepicker = (function() {

  // Variables

  let $datepicker = $('.datepicker');


  // Methods

  function init($this) {
    var options = {
      disableTouchKeyboard: true,
      autoclose: true,
      format:"yyyy-mm-dd"
    };
    $.each($this.data(), function ($key, $item) {
      options[$key] = jQuery.camelCase($item)
    })

    $this.datepicker(options);
  }


  // Events

  if ($datepicker.length) {
    $datepicker.each(function() {
      init($(this));
    });
  }

})();