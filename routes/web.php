<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/Traffic', 'ApiController@trafficIndex')->name('trafficIndex');
Route::post('/Traffic', 'ApiController@traffic')->name('traffic');
Route::get('/Conversion', 'ApiController@conversionIndex')->name('conversionIndex');
Route::post('/Conversion', 'ApiController@conversion')->name('conversion');
Route::post('/SaveResultSet', 'ApiController@saveResultSet')->name('saveResultSet');

Route::get('/User', 'UserController@index')->name('User.index');
Route::get('/User/create', 'UserController@create')->name('User.create');
Route::post('/User', 'UserController@store')->name('User.store');
Route::get('/User/{user}/edit', 'UserController@edit')->name('User.edit');
Route::put('/User/{user}', 'UserController@update')->name('User.update');
Route::get('/User/{user}', 'UserController@impersonate')->name('User.impersonate');


Route::get('/UserApi/create', 'UserApiController@create')->name('UserApi.create');
Route::post('/UserApi', 'UserApiController@store')->name('UserApi.store');
Route::get('/UserApi/{userApi}/edit', 'UserApiController@edit')->name('UserApi.edit');
Route::put('/UserApi/{userApi}', 'UserApiController@update')->name('UserApi.update');