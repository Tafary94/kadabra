<?php

namespace App;

use App\Apis\iApiAdapter;
use App\Helpers\Errors;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class UserApi extends Model
{
  use Errors;


  protected $fillable = [
    'user_id', 'api', 'url', 'username', 'password', 'api_token', 'api_token_type', 'api_token_expiration'
  ];

  protected function validationRules()
  {
    return [
      'api'=>'required',
      'url'=>'required',
      'username'=>'required|min:3|max:255',
      'password'=>'required|min:6|max:255',
    ];
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function hasValidToken()
  {
    $currentDate = Carbon::now();
    return $this->api_token && $this->api_token_expiration && $this->api_token_type
      && $currentDate->lessThan($this->api_token_expiration);
  }

  public function buildAuthorization()
  {
    return "{$this->api_token_type} {$this->api_token}";
  }

  /**
   * @param Client $client
   * @return iApiAdapter
   */
  public function getApiAdapter(Client $client)
  {
    $class = "{$this->getBaseNamespace()}\\{$this->api}";
    return new $class($this, $client);
  }

  public function reloadAdapterByModule($module, array $resultSet = [])
  {
    $moduleToClass = ucfirst($module);
    $method = "reload{$moduleToClass}Adapter";
    return $this->$method($resultSet);
  }

  public function reloadTrafficAdapter(array $resultSet = [])
  {
    $class = "{$this->getBaseNamespace()}\\Traffic";
    return $class::loadTraffic($this, $resultSet);
  }

  public function reloadConversionAdapter(array $resultSet = [])
  {
    $class = "{$this->getBaseNamespace()}\\Conversion";
    return $class::loadConversion($this, $resultSet);
  }

  private function getBaseNamespace()
  {
    return "\\App\\Apis\\{$this->api}";
  }
}
