<?php

namespace App\Http\Controllers;

use App\User;
use App\UserApi;
use Illuminate\Http\Request;

class UserApiController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, UserApi $userApi)
    {
      $user = User::find($request->get('userId'));
      if(!$user || $user->userApi){
        return redirect()
          ->route('User.index')
          ->withStatus(__('User Api already exist.'));
      }
      return view('userApi.create', compact('userApi', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, UserApi $userApi)
    {
      $User = User::find($request->get('userId'));
      if(!$User || $User->userApi){
        return redirect()
          ->route('User.index')
          ->withStatus(__('User Api already exist.'));
      }
      $userApi = new UserApi($request->all());
      if($userApi->isValid()){
        $User->userApi()->save($userApi);
        $return = redirect()
          ->route('User.index')
          ->withStatus(__('User Api successfully added.'));
      }
      else{
        $return = redirect()
          ->route('UserApi.create',['userApi' => $userApi, "userId" => $User])
          ->withErrors($userApi->errors());
      }
      return $return;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserApi  $userApi
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, UserApi $userApi)
    {
      return view('userApi.edit', compact('userApi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserApi  $userApi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserApi $userApi)
    {
      $userApi->fill($request->all());
      if($userApi->isValid()){
        $userApi->save();
        $return = redirect()
          ->route('User.index')
          ->withStatus(__('User Api successfully updated.'));
      }
      else{
        $return = redirect()
          ->route('UserApi.edit', $userApi)
          ->withErrors($userApi->errors());
      }
      return $return;
    }
}
