<?php

namespace App\Http\Controllers;

use App\LogApi;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }


  public function trafficIndex(Request $request)
  {
    return $this->traffic($request);
  }

  private function redirectToUserApiCreate()
  {
    $user = Auth::user();
    return redirect()
      ->route('UserApi.create', ['UserApi' => $user->userApi, "userId" => $user])
      ->withStatus(__('You have to add userApi credential to access to Conversion/Traffic pages'));
  }


  public function traffic(Request $request)
  {
    $api = $this->getUserApiAdapter();
    if(!$api){
      return $this->redirectToUserApiCreate();
    }
    list($brandGroupId, $fromDate, $toDate) = $this->extractSearchForm($request);
    $postRoute = route('traffic');
    $resultSet = [];
    $saveResultSet = false;
    $hasError = false;
    if($brandGroupId){
      try
      {
        $resultSet = $api->traffic($brandGroupId, Carbon::createFromTimestamp(strtotime($fromDate)), Carbon::createFromTimestamp(strtotime($toDate)));
        $saveResultSet = $resultSet->count()? $api->lastLog()->id : false;
      }
      catch (\Exception $e)
      {
        $hasError = $e->getMessage();
      }
    }
    return view('traffic', compact('brandGroupId', 'fromDate', 'toDate', 'postRoute', 'resultSet', 'saveResultSet', 'hasError'));
  }

  public function conversionIndex(Request $request)
  {
    return $this->conversion($request);
  }

  public function conversion(Request $request)
  {
    $api = $this->getUserApiAdapter();
    if(!$api){
      return $this->redirectToUserApiCreate();
    }
    list($brandGroupId, $fromDate, $toDate) = $this->extractSearchForm($request);
    $postRoute = route('conversion');
    $resultSet = [];
    $hasError = false;
    $saveResultSet = false;
    if($brandGroupId){
      try
      {
        $resultSet = $api->conversion($brandGroupId, Carbon::createFromTimestamp(strtotime($fromDate)), Carbon::createFromTimestamp(strtotime($toDate)));
        $saveResultSet = $resultSet->count()? $api->lastLog()->id : false;
      }
      catch (\Exception $e)
      {
        $hasError = $e->getMessage();
      }
    }
    return view('conversion', compact('brandGroupId', 'fromDate', 'toDate', 'postRoute', 'resultSet', 'saveResultSet', 'hasError'));
  }

  public function saveResultSet(Request $request){
    $logApiId = $request->input('logApiId', null);
    $logApi = LogApi::find($logApiId);
    $resultSet = $logApi->reloadResponse();
    DB::beginTransaction();
    $rollback = false;
    foreach ($resultSet as $row){
      $persistedModel = $row->persist();
      if(!$persistedModel && !$persistedModel->id){
        $rollback = false;
        break;
      }
    }
    if($rollback){
      DB::rollBack();
    }
    else{
      DB::commit();
    }
    $reports = [
      'success' => !$rollback,
    ];

    return response($reports);
  }

  private function getUserApiAdapter()
  {
    $userApi = Auth::user()->userApi;
    return ($userApi)? $userApi->getApiAdapter(new Client()) :null;
  }

  private function extractSearchForm(Request $request)
  {
    $params[] = $request->input('brandGroupId', null);
    $params[] = $request->input('fromDate', Carbon::today()->startOfMonth()->format('Y-m-d'));
    $params[] = $request->input('toDate', Carbon::today()->format('Y-m-d'));
    return $params;
  }
}
