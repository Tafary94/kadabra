<?php

namespace App\Http\Controllers;

use App\User;
use App\UserApi;
use Faker\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, User $user)
    {
      return view('user.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
      $faker = Factory::create();
      $user->fill($request->all());
      if($user->isValid()){
        $request->merge([
          'password' => Hash::make($request->get('password')),
          'email' => $faker->email,
        ]);
        $user->fill($request->all());
        $user->save();
        $return = redirect()
          ->route('User.index')
          ->withStatus(__('User successfully created.'));
      }
      else{
        $return = redirect()
          ->route('User.create',$user)
          ->withErrors($user->errors());
      }
      return $return;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user)
    {
      return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
      $user->fill($request->all());
      if($user->isValid()){
        $user->save();
        $return = redirect()
          ->route('User.index')
          ->withStatus(__('User successfully updated.'));
      }
      else{
        $return = redirect()
          ->route('User.edit',$user)
          ->withErrors($user->errors());
      }
      return $return;
    }

    public function impersonate(User $user)
    {
      Auth::login($user);
      return redirect()->back()->withStatus(__("You are now logged as {$user->name}"));
    }
}
