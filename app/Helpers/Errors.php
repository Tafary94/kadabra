<?php
/**
 * Created by PhpStorm.
 * User: eytan
 * Date: 7/4/20
 * Time: 9:26 PM
 */

namespace App\Helpers;


use Illuminate\Support\Facades\Validator;

trait Errors
{
  /**
   * @var \Illuminate\Validation\Validator
   */
  private $errors = null;

  public function validate()
  {
    if(!$this->errors){
      $this->errors = Validator::make($this->attributes, $this->validationRules());
      $this->errors->validate();
    }
    return $this->errors;
  }

  public function isValid(){
    return count($this->validate()->invalid()) == 0;
  }

  public function errors(){
    return $this->validate()->invalid();
  }

  protected abstract function validationRules();

}