<?php
/**
 * Created by PhpStorm.
 * User: eytan
 * Date: 7/2/20
 * Time: 4:21 PM
 */

namespace App\Helpers;


use App\LogApi;
use App\User;
use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

class LoggedGuzzleClient
{

  /**
   * @var Client
   */
  private $client;
  /**
   * @var LogApi
   */
  private $logApi;
  /**
   * @var UserApi
   */
  private $userApi;

  public function __construct(UserApi $userApi, Client $client)
  {
    $this->client = $client;
    $this->userApi = $userApi;
  }

  /**
   * @param $uri
   * @param array $options
   * @return ResponseInterface
   */
  public function post($module, $uri, array $options = []){
    $this->startLog($module, $uri, $options);
    $response = $this->client->post($uri, $options);
    $this->endLog($response);
    return $response;
  }

  private function startLog($module, $uri, array $options = []){
    $this->logApi = LogApi::create([
      'user_api_id' => $this->userApi->id,
      'api_module' => $module,
      'end_point' => $uri,
      'raw_query' => json_encode($options),
      'query_at' => Carbon::now()
    ]);
  }

  private function endLog(ResponseInterface $response){
    $this->logApi->fill([
      'raw_response' => json_encode(json_decode($response->getBody())),
      'response_headers' => json_encode($response->getHeaders()),
      'response_at' => Carbon::now()
    ])
    ->save();
  }

  public function lastLog(){
    return $this->logApi;
  }

}