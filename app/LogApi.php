<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogApi extends Model
{
  protected $fillable = [
    'user_api_id','api_module', 'end_point',
    'raw_query', 'query_at',
    'raw_response', 'response_headers', 'response_at'
  ];

  public function userApi()
  {
    return $this->belongsTo(UserApi::class);
  }

  public function reloadResponse()
  {
    if($this->api_module != 'login'){
      $resultSet = json_decode($this->raw_response, true);
      return $this->userApi->reloadAdapterByModule($this->api_module, $resultSet);
    }
    return null;
  }
}
