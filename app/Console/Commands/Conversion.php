<?php

namespace App\Console\Commands;

use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class Conversion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
  protected $signature = 'import:conversion';

    /**
     * The console command description.
     *
     * @var string
     */
  protected $description = 'Will load all userApi, call conversion api and persist them in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      try
      {
        $client = new Client();
        $userApis = UserApi::all();
        $fromDate =  Carbon::today()->subDay()->startOfDay();
        $toDate =  Carbon::today()->subDay()->endOfDay();
        foreach ($userApis as $userApi){
          $api = $userApi->getApiAdapter($client);

          $conversions = $api->conversion(1, $fromDate, $toDate);
          foreach ($conversions as $conversion){
            $conversion->persist();
          }
        }
        $this->info('import Conversion done');
      }
      catch (\Exception $e)
      {
        $this->info('import Conversion fail :' . print_r($e->getMessage(), true));
      }
    }
}
