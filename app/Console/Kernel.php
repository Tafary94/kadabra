<?php

namespace App\Console;

use App\Console\Commands\Conversion;
use App\Console\Commands\Traffic;
use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
      Traffic::class,
      Conversion::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
      $schedule->command(Traffic::class)->dailyAt('01:00')->appendOutputTo(storage_path('importTraffic.log'));
      $schedule->command(Conversion::class)->dailyAt('01:00')->appendOutputTo(storage_path('importConversion.log'));
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
