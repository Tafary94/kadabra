<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Conversion extends Model
{
  protected $fillable = [
    'user_id', 'month', 'date', 'brand', 'orientation', 'commissionOffer', 'mediaId',
    'impressions', 'uniqueImpressions', 'clicks', 'uniqueClicks', 'registrations',
    'leads', 'moneyPlayers', 'grossRevenue', 'trackingCode'
  ];

  public function scopeUser($query, User $user)
  {
    return $query->where('user_id', '=', $user->id);
  }

  public function scopeDate($query, Carbon $date)
  {
    return $query->where('date', '=', $date);
  }

  public static function getByUserAndDate(User $user, Carbon $date)
  {
    return static::User($user)->date($date)->first();
  }
}
