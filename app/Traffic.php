<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Traffic extends Model
{
  protected $fillable = [
    'user_id', 'date', 'trackingCodeDescription', 'trackingCode', 'brand', 'playerAffinity',
    'grossRevenue', 'registrations', 'leads', 'moneyPlayers', 'commissionType',
    'commissionCountry', 'playerCountry', 'anid', 'playerDevice'
  ];

  public function scopeUser($query, User $user)
  {
    return $query->where('user_id', '=', $user->id);
  }

  public function scopeDate($query, Carbon $date)
  {
    return $query->where('date', '=', $date);
  }

  public static function getByUserAndDate(User $user, Carbon $date)
  {
    return static::User($user)->date($date)->first();
  }
}
