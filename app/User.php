<?php

namespace App;

use App\Helpers\Errors;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    use Errors;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    protected function validationRules(){
      return [
        'name'=>'required|min:3|max:255',
        'username'=>'required|min:3|max:255',
        'password'=>'required|min:6|max:255',
      ];
    }

    public function conversions()
    {
      return $this->hasMany(Conversion::class);
    }

    public function traffics()
    {
      return $this->hasMany(Traffic::class);
    }

    public function scopeUsername($query, $username)
    {
      return $query->where('username', '=', $username);
    }

  /**
   * @param $login
   * @return User
   */
  public static function getByUsername($username)
  {
    return static::username($username)->first();
  }

  public function userApi()
  {
    return $this->hasOne(UserApi::class);
  }
}
