<?php
/**
 * Created by PhpStorm.
 * User: eytan
 * Date: 7/2/20
 * Time: 11:21 AM
 */

namespace App\Apis\Uffiliates;



use App\UserApi;

class Traffic extends ResponseAsModel
{

  protected $date;
  protected $trackingCodeDescription;
  protected $trackingCode;
  protected $brand;
  protected $playerAffinity;
  protected $grossRevenue;
  protected $registrations;
  protected $leads;
  protected $moneyPlayers;
  protected $commissionType;
  protected $commissionCountry;
  protected $playerCountry;
  protected $anid;
  protected $playerDevice;


  public static function loadTraffic(UserApi $userApi, array $resultSet = [])
  {
    $conversionReport = [];
    if(isset($resultSet['TrafficStatRows']) && count($resultSet['TrafficStatRows'])){
      foreach ($resultSet['TrafficStatRows'] as $row){
        $conversionReport[] = new static($userApi, $row);
      }
    }
    return collect($conversionReport);
  }

  public function persist()
  {
    return $this->user()->traffics()->create($this->toArray());
  }
}