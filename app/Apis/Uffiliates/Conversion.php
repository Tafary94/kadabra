<?php
/**
 * Created by PhpStorm.
 * User: eytan
 * Date: 7/2/20
 * Time: 2:42 PM
 */

namespace App\Apis\Uffiliates;


use App\UserApi;

class Conversion extends ResponseAsModel
{
  protected $month;
  protected $date;
  protected $brand;
  protected $orientation;
  protected $commissionOffer;
  protected $mediaId;
  protected $impressions;
  protected $uniqueImpressions;
  protected $clicks;
  protected $uniqueClicks;
  protected $registrations;
  protected $leads;
  protected $moneyPlayers;
  protected $grossRevenue;
  protected $trackingCode;


  public static function loadConversion(UserApi $userApi, array $resultSet = [])
  {
    $conversionReport = [];
    if(isset($resultSet['ConversionReportRows']) && count($resultSet['ConversionReportRows'])){
      foreach ($resultSet['ConversionReportRows'] as $row){
        $conversionReport[] = new static($userApi, $row);
      }
    }
    return collect($conversionReport);
  }

  public function persist()
  {
    return $this->user()->conversions()->create($this->toArray());
  }
}