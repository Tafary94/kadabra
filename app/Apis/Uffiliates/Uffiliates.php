<?php
/**
 * Created by PhpStorm.
 * User: eytan
 * Date: 7/2/20
 * Time: 11:21 AM
 */

namespace App\Apis\Uffiliates;


use App\Apis\iApiAdapter;
use App\Helpers\LoggedGuzzleClient;
use App\User;
use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;

class Uffiliates implements iApiAdapter
{

  /**
   * @var UserApi
   */
  private $userApi;
  /**
   * @var LoggedGuzzleClient
   */
  private $client;

  public function __construct(UserApi $userApi, Client $client)
  {

    $this->userApi = $userApi;
    $this->client = new LoggedGuzzleClient($userApi, $client);
  }

  public function traffic($brandGroupId, Carbon $fromDate, Carbon $toDate)
  {
    if(!$this->isLogged()){
      $this->login();
    }

    $response = $this->sendReportsRequests('traffic', $brandGroupId, $fromDate, $toDate);

    if($response->getStatusCode() == 200){
      $resultSet = json_decode($response->getBody(), true);
      return Traffic::loadTraffic($this->userApi ,$resultSet);
    }
    throw new UffiliateException('login Error');
  }

  public function conversion($brandGroupId, Carbon $fromDate, Carbon $toDate)
  {
    if(!$this->isLogged()){
      $this->login();
    }

    $response = $this->sendReportsRequests('conversion', $brandGroupId, $fromDate, $toDate);

    if($response->getStatusCode() == 200){
      $resultSet = json_decode($response->getBody(), true);
      return Conversion::loadConversion($this->userApi ,$resultSet);
    }
    throw new UffiliateException('login Error');
  }

  private function isLogged()
  {
    return $this->userApi->hasValidToken();
  }

  private function sendReportsRequests($module, $brandGroupId, Carbon $fromDate, Carbon $toDate)
  {
    return $this->client->post($module, $this->userApi->url. "reports/" . $module, [
      'headers' => [
        'Accept'     => 'application/json',
        'Authorization'     => "{$this->userApi->buildAuthorization()}",
      ],
      'json' => [
        'BrandGroupId' => $brandGroupId,
        'FromDate' => $fromDate->toDateTimeString(),
        'ToDate' => $toDate->toDateTimeString(),
      ]
    ]);
  }


  private function login()
  {
    $response = $this->client->post('login', $this->userApi->url. 'login', [
      'headers' => [
        'Accept'     => 'application/json',
      ],
      'json' => [
        'username' => $this->userApi->username,
        'password' => $this->userApi->password,
      ]
    ]);

    if($response->getStatusCode() == 200){
      $resultSet = json_decode($response->getBody());
      $this->userApi->fill([
        'api_token' => $resultSet->AccessToken,
        'api_token_type' => $resultSet->TokenType,
        'api_token_expiration' => Carbon::now()->addSeconds($resultSet->ExpiresIn)
        ])
        ->save();
      return true;
    }
    throw new UffiliateException('login Error');
  }

  public function lastLog(){
    return $this->client->lastLog();
  }
}