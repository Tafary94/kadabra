<?php
/**
 * Created by PhpStorm.
 * User: eytan
 * Date: 7/2/20
 * Time: 11:51 PM
 */

namespace App\Apis\Uffiliates;


use App\User;
use App\UserApi;
use Carbon\Carbon;
use Illuminate\Contracts\Support\Arrayable;

abstract class ResponseAsModel implements Arrayable
{

  /**
   * @var UserApi
   */
  protected $userApi;

  public function __construct(UserApi $userApi, $row)
  {
    foreach ($row as $key => $value){
      $key = $this->toCamelCase($key);
      if(property_exists($this, $key)){
        $this->$key = $value;
      }
    }
    $this->userApi = $userApi;
  }

  public function __call($name, $arguments = [])
  {
    return property_exists($this, $name)? $this->$name : null;
  }

  public function date()
  {
    return Carbon::createFromTimestamp(strtotime($this->date));
  }

  /**
   * Get the instance as an array.
   *
   * @return array
   */
  public function toArray()
  {
    return get_object_vars($this);
  }

  /**
   * @return User
   */
  protected function user(){
    return $this->userApi->user;
  }

  private function toCamelCase($property){
    return strtolower(substr($property, 0, 1)).substr($property, 1);
  }
}