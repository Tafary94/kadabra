<?php
/**
 * Created by PhpStorm.
 * User: eytan
 * Date: 7/3/20
 * Time: 9:29 AM
 */

namespace App\Apis;

use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;

interface iApiAdapter
{
  public function __construct(UserApi $userApi, Client $client);
  public function traffic($brandGroupId, Carbon $fromDate, Carbon $toDate);
  public function conversion($brandGroupId, Carbon $fromDate, Carbon $toDate);
  public function lastLog();
}