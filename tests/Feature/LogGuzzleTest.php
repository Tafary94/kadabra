<?php

namespace Tests\Feature;

use App\Apis\Uffiliates\Uffiliates;
use App\LogApi;
use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class LogGuzzleTest extends TestCase
{
  use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testApiCallAreLogged()
    {
      $userApi = factory(UserApi::class)->make();
      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode(
          [
            'AccessToken'=> "Tbq23mffxeZzaDQrxh+exEZtgEFkTHjjrjE5urur1TJDjxrAGneA9oNI5HqHJUWdHRZ0hF5cfIUxht+rqoZ1B2QR8BpIbqpx9co/oNm5zJ0=",
            'TokenType'=> "baerer",
            'ExpiresIn'=> 200,
          ]
        )),
        new Response(200, ['X-Foo' => 'Bar'], json_encode(['data'=>'Hello, World'])),
      ]);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $api->conversion(1, Carbon::now(), Carbon::now());
      $log = LogApi::where('user_api_id', '=', $userApi->id)->orderBy('id', 'desc')->first();
      $this->assertCount(2, LogApi::where('user_api_id', '=', $userApi->id)->get());
      $this->assertEquals($userApi->id, $log->user_api_id);
      $this->assertEquals("conversion", $log->api_module);
      $this->assertEquals(json_encode(['data'=>'Hello, World']), $log->raw_response);
    }
}
