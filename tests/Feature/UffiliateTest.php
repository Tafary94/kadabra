<?php

namespace Tests\Feature;

use App\Apis\Uffiliates\Conversion;
use App\Apis\Uffiliates\Traffic;
use App\Apis\Uffiliates\Uffiliates;
use App\LogApi;
use App\User;
use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Tests\TestCase;

class UffiliateTest extends TestCase
{
  use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSendLoginIfNoToken()
    {
      $userApi = factory(UserApi::class)->make();
      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode(
          [
            'AccessToken'=> "Tbq23mffxeZzaDQrxh+exEZtgEFkTHjjrjE5urur1TJDjxrAGneA9oNI5HqHJUWdHRZ0hF5cfIUxht+rqoZ1B2QR8BpIbqpx9co/oNm5zJ0=",
            'TokenType'=> "baerer",
            'ExpiresIn'=> 200,
          ]
        )),
        new Response(200, ['X-Foo' => 'Bar'], json_encode(['data'=>'Hello, World'])),
      ]);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $api->conversion(1, Carbon::now(), Carbon::now());
      $log = LogApi::where('user_api_id', '=', $userApi->id)->first();
      $this->assertEquals("login", $log->api_module);
    }

    public function testTokenReceiveByLoginIsPersisted()
    {
      $userApi = factory(UserApi::class)->make();
      $token = "Tbq23mffxeZzaDQrxh+exEZtgEFkTHjjrjE5urur1TJDjxrAGneA9oNI5HqHJUWdHRZ0hF5cfIUxht+rqoZ1B2QR8BpIbqpx9co/oNm5zJ0=";
      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode(
          [
            'AccessToken'=> $token,
            'TokenType'=> "baerer",
            'ExpiresIn'=> 200,
          ]
        )),
        new Response(200, ['X-Foo' => 'Bar'], json_encode(['data'=>'Hello, World'])),
      ]);
      $this->assertNull($userApi->api_token);
      $this->assertNull($userApi->api_token_expiration);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $api->conversion(1, Carbon::now(), Carbon::now());
      $log = LogApi::where('user_api_id', '=', $userApi->id)->first();
      $this->assertEquals($token, $userApi->api_token);
      $this->assertNotNull($userApi->api_token_expiration);
    }

    public function testNotSendLoginIfToken(){
      $userApi = factory(UserApi::class)->make([
        'api_token' => 'test',
        'api_token_type'=> "baerer",
        'api_token_expiration'=> Carbon::now()->addHour(),
      ]);

      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode(['data'=>'Hello, World'])),
      ]);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $api->conversion(1, Carbon::now(), Carbon::now());
      $log = LogApi::where('user_api_id', '=', $userApi->id)->first();
      $this->assertEquals($userApi->id, $log->user_api_id);
      $this->assertEquals("conversion", $log->api_module);
      $this->assertEquals(json_encode(['data'=>'Hello, World']), $log->raw_response);
    }

    public function testApiConversionReturnCollectionOfConversionObject(){
      $userApi = factory(UserApi::class)->make([
        'api_token' => 'test',
        'api_token_type'=> "baerer",
        'api_token_expiration'=> Carbon::now()->addHour(),
      ]);
      $expectedConversionRow = [
        'ConversionReportRows'=>[[
          "Month" => "2020-01-01T00:00:00",
          "Date" => null,
          "Brand" => "888sport",
          "Orientation" => "",
          "CommissionOffer" => null,
          "MediaId" => null,
          "Anid" => null,
          "Impressions" => 0,
          "UniqueImpressions" => 0,
          "Clicks" => 118,
          "UniqueClicks" => 118,
          "Registrations" => 0,
          "Leads" => 0,
          "MoneyPlayers" => 0,
          "GrossRevenue" => 0.00,
          "TrackingCode" => null
        ]]
      ];

      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode($expectedConversionRow)),
      ]);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $response = $api->conversion(1, Carbon::now(), Carbon::now());

      $this->assertInstanceOf(Collection::class, $response);
      $this->assertInstanceOf(Conversion::class, $response->first());
      $this->assertEquals(118, $response->first()->clicks());

    }

    public function testApiTrafficReturnCollectionOfTrafficObject(){
      $userApi = factory(UserApi::class)->make([
        'api_token' => 'test',
        'api_token_type'=> "baerer",
        'api_token_expiration'=> Carbon::now()->addHour(),
      ]);
      $expectedTrafficRow = [
        'TrafficStatRows'=>[[
            "Date" => "2018-10-16T00:00:00",
            "TrackingCodeDescription" => "AMZB-GB-SB-888SB-80/35%",
            "TrackingCode" => "1651669",
            "Brand" => "888sport",
            "PlayerAffinity" => "",
            "GrossRevenue" => 0.0000,
            "Registrations" => 1,
            "Leads" => 0,
            "MoneyPlayers" => 0,
            "CommissionType" => "Advantage",
            "CommissionCountry" => "All",
            "PlayerCountry" => null,
            "Anid" => null,
            "PlayerDevice" => null
        ]]
      ];

      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode($expectedTrafficRow)),
      ]);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $response = $api->traffic(1, Carbon::now(), Carbon::now());

      $this->assertInstanceOf(Collection::class, $response);
      $this->assertInstanceOf(Traffic::class, $response->first());
      $this->assertEquals("Advantage", $response->first()->commissionType());

    }

    public function testLastLog()
    {
      $userApi = factory(UserApi::class)->create();
      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode(
          [
            'AccessToken'=> "Tbq23mffxeZzaDQrxh+exEZtgEFkTHjjrjE5urur1TJDjxrAGneA9oNI5HqHJUWdHRZ0hF5cfIUxht+rqoZ1B2QR8BpIbqpx9co/oNm5zJ0=",
            'TokenType'=> "baerer",
            'ExpiresIn'=> 200,
          ]
        )),
        new Response(200, ['X-Foo' => 'Bar'], json_encode(['data'=>'Hello, World'])),
      ]);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $api->conversion(1, Carbon::now(), Carbon::now());
      $log = LogApi::orderBy('id', 'desc')->first();
      $this->assertCount(2, LogApi::where('user_api_id', '=', $userApi->id)->get());
      $this->assertEquals($log->id, $api->lastLog()->id);

    }
}
