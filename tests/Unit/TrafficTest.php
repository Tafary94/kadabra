<?php

namespace Tests\Unit;

use App\Traffic;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TrafficTest extends TestCase
{
  use DatabaseTransactions;

  public function testGetByUserAndDate()
  {
    $user = factory(User::class)->create();
    $date = Carbon::createFromDate('2020', '01', '01');
    $expectedTraffic = factory(Traffic::class)->create(['user_id' => $user->id, 'date' => $date]);

    $traffic = Traffic::getByUserAndDate($user, $date);

    $this->assertEquals($expectedTraffic->id, $traffic->id);
  }

}
