<?php

namespace Tests\Unit;

use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Apis\Uffiliates\Conversion;
use App\Apis\Uffiliates\Traffic;
use App\Apis\Uffiliates\Uffiliates;

class UserTest extends TestCase
{
  use DatabaseTransactions;
  /**
   * A basic test example.
   *
   * @return void
   */
  public function testGetByUsername()
  {
    $user = factory(User::class)->create();

    $userByUsername = User::getByUsername($user->username);

    $this->assertEquals($user->id, $userByUsername->id);

  }

}
