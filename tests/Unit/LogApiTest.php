<?php

namespace Tests\Unit;

use App\Apis\Uffiliates\Conversion;
use App\Apis\Uffiliates\Traffic;
use App\Apis\Uffiliates\Uffiliates;
use App\LogApi;
use App\User;
use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Collection;
use Tests\TestCase;

class LogApiTest extends TestCase
{
  use DatabaseTransactions;

    public function testReloadConversion(){
      $user = factory(UserApi::class)->create();
      $userApi = factory(UserApi::class)->create([
        'user_id' => $user->id,
        'api_token' => 'test',
        'api_token_type'=> "baerer",
        'api_token_expiration'=> Carbon::now()->addHour(),
      ]);
      $expectedConversionRow = [
        'ConversionReportRows'=>[[
          "Month" => "2020-01-01T00:00:00",
          "Date" => null,
          "Brand" => "888sport",
          "Orientation" => "",
          "CommissionOffer" => null,
          "MediaId" => null,
          "Anid" => null,
          "Impressions" => 0,
          "UniqueImpressions" => 0,
          "Clicks" => 118,
          "UniqueClicks" => 118,
          "Registrations" => 0,
          "Leads" => 0,
          "MoneyPlayers" => 0,
          "GrossRevenue" => 0.00,
          "TrackingCode" => null
        ]]
      ];

      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode($expectedConversionRow)),
      ]);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $api->conversion(1, Carbon::now(), Carbon::now());

      $reloadResponse = $api->lastLog()->reloadResponse();

      $this->assertInstanceOf(Collection::class, $reloadResponse);
      $this->assertInstanceOf(Conversion::class, $reloadResponse->first());
      $this->assertEquals(118, $reloadResponse->first()->clicks());

    }

    public function testReloadTraffic(){
      $user = factory(UserApi::class)->create();
      $userApi = factory(UserApi::class)->create([
        'user_id' => $user->id,
        'api_token' => 'test',
        'api_token_type'=> "baerer",
        'api_token_expiration'=> Carbon::now()->addHour(),
      ]);
      $expectedTrafficRow = [
        'TrafficStatRows'=>[[
            "Date" => "2018-10-16T00:00:00",
            "TrackingCodeDescription" => "AMZB-GB-SB-888SB-80/35%",
            "TrackingCode" => "1651669",
            "Brand" => "888sport",
            "PlayerAffinity" => "",
            "GrossRevenue" => 0.0000,
            "Registrations" => 1,
            "Leads" => 0,
            "MoneyPlayers" => 0,
            "CommissionType" => "Advantage",
            "CommissionCountry" => "All",
            "PlayerCountry" => null,
            "Anid" => null,
            "PlayerDevice" => null
        ]]
      ];

      $guzzleClientMock = new MockHandler([
        new Response(200, ['X-Foo' => 'Bar'], json_encode($expectedTrafficRow)),
      ]);

      $handlerStack = HandlerStack::create($guzzleClientMock);
      $client = new Client(['handler' => $handlerStack]);

      $api = new Uffiliates($userApi, $client);

      $api->traffic(1, Carbon::now(), Carbon::now());

      $reloadResponse = $api->lastLog()->reloadResponse();

      $this->assertInstanceOf(Collection::class, $reloadResponse);
      $this->assertInstanceOf(Traffic::class, $reloadResponse->first());
      $this->assertEquals("Advantage", $reloadResponse->first()->commissionType());

    }

}
