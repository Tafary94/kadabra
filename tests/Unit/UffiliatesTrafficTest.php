<?php

namespace Tests\Unit;

use App\Apis\Uffiliates\Traffic;
use App\User;
use App\UserApi;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UffiliatesTrafficTest extends TestCase
{

  use DatabaseTransactions;

  public function testDate()
  {
    $row = [
      "Date" => "2018-10-16T00:00:00",
      "TrackingCodeDescription" => "AMZB-GB-SB-888SB-80/35%",
      "TrackingCode" => "1651669",
      "Brand" => "888sport",
      "PlayerAffinity" => "",
      "GrossRevenue" => 0.0000,
      "Registrations" => 1,
      "Leads" => 0,
      "MoneyPlayers" => 0,
      "CommissionType" => "Advantage",
      "CommissionCountry" => "All",
      "PlayerCountry" => null,
      "Anid" => null,
      "PlayerDevice" => null
    ];
    $userApi = factory(UserApi::class)->make();
    $traffic = new Traffic($userApi, $row);

    $this->assertEquals(Carbon::createFromTimestamp(strtotime("2018-10-16T00:00:00")), $traffic->date());
  }

  public function testPersistWhenNotAlreadySave()
  {
    $row = [
      "Date" => "2018-10-16T00:00:00",
      "TrackingCodeDescription" => "AMZB-GB-SB-888SB-80/35%",
      "TrackingCode" => "1651669",
      "Brand" => "888sport",
      "PlayerAffinity" => "",
      "GrossRevenue" => 0.0000,
      "Registrations" => 1,
      "Leads" => 0,
      "MoneyPlayers" => 0,
      "CommissionType" => "Advantage",
      "CommissionCountry" => "All",
      "PlayerCountry" => null,
      "Anid" => null,
      "PlayerDevice" => null
    ];
    $user = factory(User::class)->create();
    $userApi = factory(UserApi::class)->create(['user_id' => $user->id]);
    $traffic = new Traffic($userApi, $row);

    $persistedTraffic = $traffic->persist();

    $this->assertNotNull($persistedTraffic);
    $this->assertNotNull($persistedTraffic->id);
  }

}
