<?php

namespace Tests\Unit;

use App\Conversion;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class ConversionTest extends TestCase
{
  use DatabaseTransactions;
  /**
   * A basic test example.
   *
   * @return void
   */
  public function testGetByUserAndDate()
  {
    $user = factory(User::class)->create();
    $date = Carbon::createFromDate('2020', '01', '01');
    $expectedConversion = factory(Conversion::class)->create(['user_id' => $user->id, 'date' => $date]);

    $conversion = Conversion::getByUserAndDate($user, $date);

    $this->assertEquals($expectedConversion->id, $conversion->id);
  }

}
