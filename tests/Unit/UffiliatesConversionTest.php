<?php

namespace Tests\Unit;

use App\Apis\Uffiliates\Conversion;
use App\User;
use App\UserApi;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UffiliatesConversionTest extends TestCase
{

  use DatabaseTransactions;

  public function testDate()
  {
    $row = [
      "Month" => "2020-01-01T00:00:00",
      "Date" => "2020-01-01T00:00:00",
      "Brand" => "888sport",
      "Orientation" => "",
      "CommissionOffer" => null,
      "MediaId" => null,
      "Anid" => null,
      "Impressions" => 0,
      "UniqueImpressions" => 0,
      "Clicks" => 118,
      "UniqueClicks" => 118,
      "Registrations" => 0,
      "Leads" => 0,
      "MoneyPlayers" => 0,
      "GrossRevenue" => 0.00,
      "TrackingCode" => null
    ];
    $userApi = factory(UserApi::class)->make();
    $conversion = new Conversion($userApi, $row);

    $this->assertEquals(Carbon::createFromTimestamp(strtotime("2020-01-01T00:00:00")), $conversion->date());
  }

  public function testPersistWhenNotAlreadySave()
  {
    $row = [
      "Month" => "2020-01-01T00:00:00",
      "Date" => null,
      "Brand" => "888sport",
      "Orientation" => "",
      "CommissionOffer" => null,
      "MediaId" => null,
      "Anid" => null,
      "Impressions" => 0,
      "UniqueImpressions" => 0,
      "Clicks" => 118,
      "UniqueClicks" => 118,
      "Registrations" => 0,
      "Leads" => 0,
      "MoneyPlayers" => 0,
      "GrossRevenue" => 0.00,
      "TrackingCode" => null
    ];
    $user = factory(User::class)->create();
    $userApi = factory(UserApi::class)->create(['user_id' => $user->id]);
    $conversion = new Conversion($userApi, $row);

    $persistedConversion = $conversion->persist();

    $this->assertNotNull($persistedConversion);
    $this->assertNotNull($persistedConversion->id);
  }

}
