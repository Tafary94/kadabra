<?php

namespace Tests\Unit;

use App\Apis\Uffiliates\Conversion;
use App\Apis\Uffiliates\Traffic;
use App\Apis\Uffiliates\Uffiliates;
use App\UserApi;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class UserApiTest extends TestCase
{
  use DatabaseTransactions;

    public function testHasValidTokenWhenNoToken()
    {
      $userApi = factory(UserApi::class)->make();
      $this->assertFalse($userApi->hasValidToken());
    }

    public function testHasValidTokenNoTokenType()
    {
      $userApi = factory(UserApi::class)->make([
        'api_token' => 'test'
      ]);
      $this->assertFalse($userApi->hasValidToken());
    }

    public function testHasValidTokenNoTokenExpiration()
    {
      $userApi = factory(UserApi::class)->make([
        'api_token' => 'test',
        'api_token_type' => 'Bearer',
      ]);
      $this->assertFalse($userApi->hasValidToken());
    }

    public function testHasValidTokenTokenExpirationExpired()
    {
      $userApi = factory(UserApi::class)->make([
        'api_token' => 'test',
        'api_token_type' => 'Bearer',
        'api_token_expiration' => '2018-01-01 00:00:00',
      ]);
      $this->assertFalse($userApi->hasValidToken());
    }

    public function testHasValidToken()
    {
      $userApi = factory(UserApi::class)->make([
        'api_token' => 'test',
        'api_token_type' => 'Bearer',
        'api_token_expiration'=> Carbon::now()->addHour(),
      ]);
      $this->assertTrue($userApi->hasValidToken());
    }

    public function testBuildAuthorization()
    {
      $userApi = factory(UserApi::class)->make([
        'api_token' => 'test',
        'api_token_type' => 'Bearer',
        'api_token_expiration'=> Carbon::now()->addHour(),
      ]);
      $this->assertEquals("Bearer test", $userApi->buildAuthorization());
    }

    public function testGetApiAdapter()
    {
      $userApi = factory(UserApi::class)->make();
      $client = new Client();
      $this->assertInstanceOf(Uffiliates::class, $userApi->getApiAdapter($client));
    }

    public function testreloadConversionAdapter()
    {
      $userApi = factory(UserApi::class)->make();
      $resultSet = [
        'ConversionReportRows'=>[[
          "Month" => "2020-01-01T00:00:00",
          "Date" => null,
          "Brand" => "888sport",
          "Orientation" => "",
          "CommissionOffer" => null,
          "MediaId" => null,
          "Anid" => null,
          "Impressions" => 0,
          "UniqueImpressions" => 0,
          "Clicks" => 118,
          "UniqueClicks" => 118,
          "Registrations" => 0,
          "Leads" => 0,
          "MoneyPlayers" => 0,
          "GrossRevenue" => 0.00,
          "TrackingCode" => null
        ]]
      ];
      $this->assertInstanceOf(Conversion::class, $userApi->reloadConversionAdapter($resultSet)->first());
    }

    public function testreloadTrafficAdapter()
    {
      $userApi = factory(UserApi::class)->make();
      $resultSet = [
        'TrafficStatRows'=>[[
          "Date" => "2018-10-16T00:00:00",
          "TrackingCodeDescription" => "AMZB-GB-SB-888SB-80/35%",
          "TrackingCode" => "1651669",
          "Brand" => "888sport",
          "PlayerAffinity" => "",
          "GrossRevenue" => 0.0000,
          "Registrations" => 1,
          "Leads" => 0,
          "MoneyPlayers" => 0,
          "CommissionType" => "Advantage",
          "CommissionCountry" => "All",
          "PlayerCountry" => null,
          "Anid" => null,
          "PlayerDevice" => null
        ]]
      ];
      $this->assertInstanceOf(Traffic::class, $userApi->reloadTrafficAdapter($resultSet)->first());
    }
}
