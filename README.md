# README #

Thank you for the consideration, this is my solution for the test you give me : 

    * You have to create a little platform in PHP (using Laravel if possible) which will allow us to login and use 888's APIs
    * User must be able to choose the API he wants and has to fill his IDs to connect to 888's system
    * User must be able to enter the required parameters for API use and retrieve data from the chosen API
    * You have to display the retrieved data from the API
    * User must be able to import the retrieved data from the API in a DB
    * Extra task – Set up tasks to automatically import data in a DB from 888's APIs (using Laravel CRON system if possible)


### My Solution ###

* I create a username/password platform where user can connect and retrieve datas from 888 api
* You can add other user and userApi (on the "user" nav and his api there) and impersonate(log as) directly from the user page
* I develop the project so you can add other api by adding folder on App\Apis and add decorators to make it work with the platform model
* I add the date on the "conversion/traffic" form in order to have some data from the test account (no data after october 2019)
* I log every request for debugging 
* you can retrieve the objects from the log exactly as you just send a api request
* If datas are present on the "conversion/traffic" a "save" button will be appear
* I had two new artisan commands for importing datas from "conversion/traffic", and use them for the cron
* All my code is tested with phpUnit

### How do I get set up? ###

* execute `git clone https://Tafary94@bitbucket.org/Tafary94/kadabra.git`
* Create a database and set in .env file 
* execute `composer install`
* execute `php artisan migrate`
* execute `php artisan db:seed`
* execute `php artisan serve`
* open a browser and go to `http://localhost:8000`
* use the username/password you give me for the test (don't forget to whitelist ip in https://program.uffiliates.com/en/apidetails)

### Test with unit test ###

* Run `php vendor/bin/phpunit --debug`