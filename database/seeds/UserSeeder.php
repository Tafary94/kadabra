<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(\App\User::class)->create([
        'name' => 'Daniel Klein',
        'username' => 'romeosab',
        'email' => 'daniel.klein@888holdings.com',
        'email_verified_at' => Carbon::now(),
        'password' => '$2y$10$m6bSomdGt3beR.M/.uMrI.mcG2uN3XqL144d2p6lLAaSRcb4gHlxa'
      ]);
    }
}
