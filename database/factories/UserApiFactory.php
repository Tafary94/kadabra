<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\UserApi::class, function (Faker $faker) {
  return [
    'id' => $faker->numberBetween(),
    'user_id' => factory(App\User::class)->make()->id,
    'api' => 'Uffiliates',
    'url' => 'http://localhost/',
    'username' => 'test',
    'password' => 'test', // password
    'api_token' => null,
    'api_token_type' => null,
    'api_token_expiration' => null,
  ];
});
