<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrafficTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->dateTime('date')->nullable();
            $table->string('trackingCodeDescription')->nullable();
            $table->string('trackingCode')->nullable();
            $table->string('brand')->index()->nullable();
            $table->string('playerAffinity')->nullable();
            $table->decimal('grossRevenue', 8, 4)->default(0);
            $table->integer('registrations')->default(0);
            $table->integer('leads')->default(0);
            $table->integer('moneyPlayers')->default(0);
            $table->string('commissionType')->nullable();
            $table->string('commissionCountry')->nullable();
            $table->string('playerCountry')->nullable();
            $table->string('anid')->nullable();
            $table->string('playerDevice')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffic');
    }
}
