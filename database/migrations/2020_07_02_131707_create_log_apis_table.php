<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogApisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_apis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_api_id')->index();
            $table->string('api_module')->index();
            $table->string('end_point')->index();
            $table->longText('raw_query');
            $table->dateTime('query_at');
            $table->longText('raw_response')->nullable();
            $table->longText('response_headers')->nullable();
            $table->dateTime('response_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_apis');
    }
}
