<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->dateTime('month')->index()->nullable();
            $table->dateTime('date')->nullable();
            $table->string('brand')->index()->nullable();
            $table->string('orientation')->nullable();
            $table->string('commissionOffer')->nullable();
            $table->string('mediaId')->index()->nullable();
            $table->integer('impressions')->default(0);
            $table->integer('uniqueImpressions')->default(0);
            $table->integer('clicks')->default(0);
            $table->integer('uniqueClicks')->default(0);
            $table->integer('registrations')->default(0);
            $table->integer('leads')->default(0);
            $table->integer('moneyPlayers')->default(0);
            $table->double('grossRevenue')->default(0);
            $table->string('trackingCode')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversions');
    }
}
