@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <dov class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('layouts.navbar')
                    </div>
                </div>
            </dov>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('Conversion') }}</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                @include('layouts.searchbar', compact('fromDate', 'toDate', 'postRoute', 'saveResultSet'))
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            @if ($hasError)
                                <div class="alert alert-danger" role="alert">
                                    {{ $hasError }}
                                </div>
                            @endif
                            <div class="col-md-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Month</th>
                                        <th>Date</th>
                                        <th>Brand</th>
                                        <th>Orientation</th>
                                        <th>Commission Offer</th>
                                        <th>Media Id</th>
                                        <th>Impressions</th>
                                        <th>Unique Impressions</th>
                                        <th>Clicks</th>
                                        <th>Unique Clicks</th>
                                        <th>Registrations</th>
                                        <th>Leads</th>
                                        <th>Money Players</th>
                                        <th>Gross Revenue</th>
                                        <th>Tracking Code</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($resultSet as $row)
                                        <tr>
                                            <td>{{ $row->month() }}</td>
                                            <td>{{ $row->date() }}</td>
                                            <td>{{ $row->brand() }}</td>
                                            <td>{{ $row->orientation() }}</td>
                                            <td>{{ $row->commissionOffer() }}</td>
                                            <td>{{ $row->mediaId() }}</td>
                                            <td>{{ $row->impressions() }}</td>
                                            <td>{{ $row->uniqueImpressions() }}</td>
                                            <td>{{ $row->clicks() }}</td>
                                            <td>{{ $row->uniqueClicks() }}</td>
                                            <td>{{ $row->registrations() }}</td>
                                            <td>{{ $row->leads() }}</td>
                                            <td>{{ $row->moneyPlayers() }}</td>
                                            <td>{{ $row->grossRevenue() }}</td>
                                            <td>{{ $row->trackingCode() }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
