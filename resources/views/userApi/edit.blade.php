@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <dov class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('layouts.navbar')
                    </div>
                </div>
            </dov>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('User Api') }}</div>
                    <div class="panel-body">

                        <form method="post" action="{{ route('UserApi.update', $userApi) }}" autocomplete="off" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="align-items-center">

                                @include('layouts.form.regular.select',['label'=>'Api','field'=>'api','value'=>$userApi->api,'options'=>[['value'=>'Uffiliates', 'name'=>'Uffiliates']]])
                                @include('layouts.form.regular.input',['label'=>'Api Url','field'=>'url','value'=>$userApi->url,'required'=>true])
                                @include('layouts.form.regular.input',['label'=>'Api Username','field'=>'username','value'=>$userApi->username,'required'=>true])
                                @include('layouts.form.regular.input',['label'=>'Api Password','field'=>'password','value'=>$userApi->password,'required'=>true])

                                <div class="col-auto">
                                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
