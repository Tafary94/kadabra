@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <dov class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('layouts.navbar')
                    </div>
                </div>
            </dov>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ __('User') }}</div>
                    <div class="panel-body">

                        <form method="post" action="{{ route('User.store', $user) }}" autocomplete="off" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="align-items-center">

                                @include('layouts.form.regular.input',['label'=>'Name','field'=>'name','value'=>$user->name,'required'=>true])
                                @include('layouts.form.regular.input',['label'=>'UserName','field'=>'username','value'=>$user->username,'required'=>true])
                                @include('layouts.form.regular.input',['label'=>'Password','field'=>'password','value'=>$user->password,'required'=>true])

                                <div class="col-auto">
                                    <button type="submit" class="btn btn-success">{{ __('Save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
