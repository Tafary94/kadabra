@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <dov class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-body">
                        @include('layouts.navbar')
                    </div>
                </div>
            </dov>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ __('User') }}
                        <a href="{{ route('User.create') }}" class="btn btn-success ml-2">Add New User</a>
                    </div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>
                                        <a href="{{ route('User.edit', $user) }}" class="btn btn-primary">Edit User</a>
                                        @if($user->userApi)
                                            <a href="{{ route('UserApi.edit', ['UserApi' => $user->userApi, "userId" => $user]) }}" class="btn btn-warning">Edit Api</a>
                                        @else
                                            <a href="{{ route('UserApi.create', ['UserApi' => $user->userApi, "userId" => $user]) }}" class="btn btn-success">Add Api</a>
                                        @endIf
                                        @if(Auth::user()->id != $user->id)
                                            <a href="{{ route('User.impersonate', ['user' => $user]) }}" class="btn btn-danger">Log as</a>
                                        @endIf
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
