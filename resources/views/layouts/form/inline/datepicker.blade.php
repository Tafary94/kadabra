<div class="align-items-center{{ $errors->has($field) ? ' has-danger' : '' }}">
    <div class="col-auto">
        <div class="input-group input-group-alternative">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input
                    {{ (isset($startDate))? 'data-start-date='.$startDate.'' : '' }}
                    {{ (isset($endDate))? 'data-end-date='.$endDate.'' : '' }}
                    name="{{ $field }}"
                    id="input-{{ $field }}"
                    class="form-control datepicker form-control-alternative{{ $errors->has($field) ? ' is-invalid' : '' }}"
                    placeholder="{{ __($label) }}"
                    value="{{ old($field, $value) }}"
                    {{ (isset($required))? 'required' : '' }}
            >

            @if ($errors->has($field))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first($field) }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>

