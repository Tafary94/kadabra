<div class="align-items-center{{ $errors->has($field) ? ' has-danger' : '' }}">
    <div class="col-auto">
        <input
                type="{{ ($field== 'password')? 'password' : 'text' }}"
                name="{{ $field }}"
                id="input-{{ $field }}"
                class="form-control form-control-alternative{{ $errors->has($field) ? ' is-invalid' : '' }}"
                placeholder="{{ __($label) }}"
                value="{{ old($field, $value) }}"
                {{ (isset($required))? 'required' : '' }}
        >

        @if ($errors->has($field))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first($field) }}</strong>
            </span>
        @endif
    </div>
</div>