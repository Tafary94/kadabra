<div class="form-group{{ $errors->has($field) ? ' has-danger' : '' }}">
    @include("layouts.form.regular.label")
    <select
            name="{{ $field }}"
            id="input-{{ $field }}"
            class="form-control-chosen form-control form-control-alternative{{ $errors->has($field) ? ' is-invalid' : '' }}"
            {{ (isset($required))? 'required' : '' }}
            {{ (isset($multipe))? 'multiple' : '' }}
    >

        @if(isset($required) || isset($all))
            <option value="">--</option>
        @endif
        @foreach($options as $option)
            <option value="{{$option['value']}}" {{ ($value == $option['value'])? 'selected="selected"' : '' }}>{{$option['name']}}</option>
        @endforeach
    </select>

    @if ($errors->has($field))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first($field) }}</strong>
        </span>
    @endif
</div>