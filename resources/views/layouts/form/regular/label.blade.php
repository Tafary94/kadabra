
<label class="form-control-label" for="input-name">
    {{ __($label) }}
    @if(isset($required))
        <span style="color:red">*</span>
    @endif
    @if(isset($help))
                <i class="fa fa-question-circle" aria-hidden="true" data-html="true" data-trigger="hover" data-placement="right" data-toggle="tooltip" data-container="body" data-original-title="{{$help}}"></i>
    @endif
</label>