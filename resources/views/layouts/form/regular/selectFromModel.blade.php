<div class="form-group{{ $errors->has($field) ? ' has-danger' : '' }}">
    @include("layouts.form.regular.label")
    <select
            name="{{ $field }}"
            id="input-{{ $field }}"
            class="{{ (isset($choosen))? 'form-control-chosen ' : '' }}form-control form-control-alternative{{ $errors->has($field) ? ' is-invalid' : '' }} input-{{ $field }}"
            {{ (isset($required))? 'required' : '' }}
            {{ (isset($multiple))? 'multiple' : '' }}
            {{ (isset($onChange))? 'onchange="'. $onChange .'"' : '' }}
    >

        @if(isset($required) || isset($all))
            <option value="">--</option>
        @endif
        @foreach($options as $option)
            <option value="{{$option->id}}" {{ ($value == $option->id || ( is_array($value) && (in_array($option->id, $value))))? 'selected="selected"' : '' }}>{{$option->name}}</option>
        @endforeach
    </select>

    @if ($errors->has($field))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first($field) }}</strong>
        </span>
    @endif
</div>