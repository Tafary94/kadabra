<div class="form-group{{ $errors->has($field) ? ' has-danger' : '' }}">
    @include("layouts.form.regular.label")
    <textarea
            @if(isset($datasAttr))
            @foreach($datasAttr as $key=>$data)
                    data-{{$key}}="{{$data}}"
            @endforeach
            @endif
            name="{{ $field }}"
            id="input-{{ $field }}"
            class="form-control form-control-alternative{{ $errors->has($field) ? ' is-invalid' : '' }} input-{{ $field }}"
            placeholder="{{ __($label) }}"
            {{ (isset($required))? 'required' : '' }}
    >{{ old($field, $value) }}</textarea>
    @if ($errors->has($field))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first($field) }}</strong>
        </span>
    @endif
</div>