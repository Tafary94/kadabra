<input
        type="hidden"
        name="{{ $field }}"
        id="input-{{ $field }}"
        value="{{ old($field, $value) }}"
>