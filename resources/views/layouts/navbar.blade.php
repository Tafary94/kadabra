
<div class="container-fluid">
    <div>
        <a href="{{ route('trafficIndex') }}">
            <span>{{ __('Traffic') }}</span>
        </a>
    </div>
    <div>
        <a href="{{ route('conversionIndex') }}">
            <span>{{ __('Conversion') }}</span>
        </a>
    </div>
    <hr>
    <div>
        <a href="{{ route('User.index') }}">
            <span>{{ __('User') }}</span>
        </a>
    </div>

</div>