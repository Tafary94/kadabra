
<form method="post" action="{{ $postRoute }}" autocomplete="off" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-row align-items-center">

        @include('layouts.form.inline.input',['label'=>'Brand Group Id','field'=>'brandGroupId','value'=>$brandGroupId,'required'=>true])
        @include('layouts.form.inline.datepicker',['label'=>'From Date','field'=>'fromDate','value'=>$fromDate,'required'=>true])
        @include('layouts.form.inline.datepicker',['label'=>'To Date','field'=>'toDate','value'=>$toDate,'required'=>true])

        <div class="col-auto">
            <button type="submit" class="btn btn-success">{{ __('Send') }}</button>
        </div>
        <div class="col-auto">
            @if($saveResultSet != false)
                <a onclick='saveResultSet({{$saveResultSet}}, "{{ route('saveResultSet') }}", "{{ csrf_token() }}");return false;' class="btn btn-warning">{{ __('Save') }}</a>
            @endif
        </div>
    </div>
</form>