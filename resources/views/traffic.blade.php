@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <dov class="col-md-2">
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('layouts.navbar')
                </div>
            </div>
        </dov>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">{{ __('Traffic') }}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-md-12">
                            @include('layouts.searchbar', compact('fromDate', 'toDate', 'postRoute', 'saveResultSet'))
                        </div>
                    </div>
                        <hr>
                    <div class="row">
                        @if ($hasError)
                            <div class="alert alert-danger" role="alert">
                                {{ $hasError }}
                            </div>
                        @endif
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Tracking Code Description</th>
                                        <th>Tracking Code</th>
                                        <th>Brand</th>
                                        <th>Player Affinity</th>
                                        <th>Gross Revenue</th>
                                        <th>Registrations</th>
                                        <th>Leads</th>
                                        <th>Money Players</th>
                                        <th>Commission Type</th>
                                        <th>Commission Country</th>
                                        <th>Player Country</th>
                                        <th>Anid</th>
                                        <th>Player Device</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($resultSet as $row)
                                    <tr>
                                        <td>{{ $row->date() }}</td>
                                        <td>{{ $row->trackingCodeDescription() }}</td>
                                        <td>{{ $row->trackingCode() }}</td>
                                        <td>{{ $row->brand() }}</td>
                                        <td>{{ $row->playerAffinity() }}</td>
                                        <td>{{ $row->grossRevenue() }}</td>
                                        <td>{{ $row->registrations() }}</td>
                                        <td>{{ $row->leads() }}</td>
                                        <td>{{ $row->moneyPlayers() }}</td>
                                        <td>{{ $row->commissionType() }}</td>
                                        <td>{{ $row->commissionCountry() }}</td>
                                        <td>{{ $row->playerCountry() }}</td>
                                        <td>{{ $row->anid() }}</td>
                                        <td>{{ $row->playerDevice() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
